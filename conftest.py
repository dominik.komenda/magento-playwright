import pytest
from playwright.sync_api import Page
@pytest.fixture
def _page(page: Page):
    page.goto("https://monitory.mastiff.pl")

    yield page