import re
from playwright.sync_api import expect
from frontend.locators.Locators import GenericLocators as locate
from config.Commons import commons

def test_login(_page):
    _page.get_by_title(locate.LOGIN).click()
    _page.get_by_role('textbox', name='E-mail').fill(commons.get('user'))
    _page.get_by_label('Hasło').fill(commons.get('password'))
    _page.get_by_role('checkbox').uncheck()
    #_page.locator("//button [contains(@name,'send')][1]").click()
    _page.get_by_role('button', name='Zaloguj się').click()

    expect(_page).to_have_url('http://monitory.mastiff.pl/customer/account/')